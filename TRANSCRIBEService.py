import os
import sys
import argparse
import time
from flask_cors import CORS
from flask import Flask, request, make_response, render_template, redirect, url_for
import transcribe
import threading
import uuid

app = Flask(__name__)
CORS(app)

class proc_inst:
    def __init__(self, pid, model, input_dir, output_dir):
        self.pid = pid
        self.model = model
        self.input_dir = input_dir
        self.output_dir = output_dir
        self.node_list = None

process_instances = {}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload():
    temp_dir = '.\\temp' if os.name == 'nt' else './temp'
    if not os.path.isdir(temp_dir):
        os.mkdir(temp_dir)

    if request.method == 'POST':
        pid = str(uuid.uuid1())
        model = request.form['model']
        input_dir = request.form['input_dir']
        output_dir = request.form['output_dir']
        info = [pid, model, input_dir, output_dir]
        tempjson = os.path.join(temp_dir, pid+'.json')
        
        thrd = threading.Thread(target=run_transcribe, args=(model, input_dir, output_dir, tempjson))
        process_instances.update({ pid: proc_inst(pid, model, input_dir, output_dir) })
        thrd.start()

        # scrape temp json file and convert to node list
        if os.path.isfile(tempjson):
            process_instances[pid].node_list = transcribe.read_tempjson(tempjson) 
        
        return render_template('table.html', info=info, files=process_instances[pid].node_list)

@app.route('/profile', methods=['POST'])
def check_pid():
    if request.method == 'POST':
        pid = request.form['pid']
        
        if pid not in process_instances:
            return redirect(url_for('index'))
        
        temp_dir = '.\\temp' if os.name == 'nt' else './temp'
        if not os.path.isdir(temp_dir):
            os.mkdir(temp_dir)
            
        tempjson = os.path.join(temp_dir, pid+'.json')
        # scrape temp json file and convert to node list
        if os.path.isfile(tempjson):
            process_instances[pid].node_list = transcribe.read_tempjson(tempjson) 

        proc = process_instances[pid]

        info = [pid, proc.model, proc.input_dir, proc.output_dir]
        files = proc.node_list

        return render_template('table.html', info=info, files=files)

    return redirect(url_for('index'))


def run_transcribe(model, input_dir, output_dir, tempjson):
    # upload the input files to api
    node_list = transcribe.upload_for_transcription(input_dir, model, tempjson)
    # get the xml data for each job
    node_list = transcribe.get_transcript(node_list, output_dir, tempjson)

if __name__ == '__main__':
    if os.getenv("DEBUGGER"):
        import debugpy
        debugpy.listen(('localhost',5678))
        debugpy.wait_for_client()

    app.run(host='localhost',port=5033)
