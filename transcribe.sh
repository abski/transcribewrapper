#!/bin/bash

if [ $# -lt 1 ]
then
  echo "ASR model required"
  exit  1
fi

if [ $# -lt 2 ]
then
  echo "Input directory required"
  exit  1
fi

if [ $# -lt 3 ]
then
  echo "Output directory required"
  exit  1
fi

if [ $# -gt 3 ]
then
  echo "Too many arguments"
  exit  1
fi

source ./transcribeEnv/bin/activate

exec python transcribe.py --model="$1" --input="$2" --output="$3" &
