from os import listdir, mkdir
from os.path import isfile, isdir, join
import time
import subprocess
import requests, argparse, json, threading

API_KEY = '912f0636-a7a5-4029-9c94-290cbe9b7bd1'

class dataNode:
    def __init__(self, fileDir, filename):
        self.fileDir = fileDir
        self.filename = filename
        self.job_id = None
        self.xml_data = None
        self.upload_status = None
        self.upload_error = None
        self.download_status = None
        self.download_error = None
    

def upload_for_transcription(fldr, model, monitor_file):
    """Sends requests to API to transcribe the files in the folder(fldr) given with the model.
    
    Creates a dataNode object list and sends a request for each node to the API. 
    If the request was successful, the job ID is saved into dataNode that it belongs to. 
    If failed, the response is stored in f.error.

    Returns: node_list (list)
    A list, the length of the number of files in the fldr, with a dataNode per file.
    """
    node_list = [dataNode(fldr, f) for f in listdir(fldr) if isfile(join(fldr, f))]

    url = "https://api.apptek.com/api/v2/transcribe/" + model + "?[punc=1]&[diar=1]"
    headers = {"content-type": "audio/wav", 'x-token': API_KEY}
    
    for f in node_list:
        with open(f.fileDir +'/'+ f.filename, 'rb+') as fp:
            payload = fp
            response = requests.request("POST", url, headers=headers, data=payload)
            f.upload_status = response.status_code
            if response.status_code == 200:
                print(f'{f.filename}: {response.text}\n{f.filename}: UPLOAD SUCCESS\n')
                f.upload_status = response.status_code
                f.job_id = extract_jobid(response.text)
            else:
                print('{f.filename}: {response.text}\n{f.filename}: UPLOAD FAILED\n')
                f.upload_error = response
        
        if monitor_file != None:
            with threading.Lock():
                write_nodes(monitor_file, node_list)
    
    return node_list


def extract_jobid(res_text):
    """Helper function for upload_for_transcription that takes the substring of a response.text (string) where the job ID should be. 

    Returns: substring on res_text (string)
    A substring that that only includes the request id from the response.text from an HTTP response. 
    """
    if res_text[11:15] == "true":
        i = 29 
        for ch in res_text[30:]:
            i += 1
            if ch == "\"":
                break
    return res_text[30:i]
        

def get_transcript(node_list, out_dir, monitor_file):
    done = False

    if not isdir(out_dir):
        mkdir(out_dir)

    while not done:
        time.sleep(60)
        done = True
    
        for f in node_list:
            # if upload returned error, skip
            if f.upload_error is not None:
                continue
            
            # if download returns anything but 204, skip
            if f.download_status is not None:
                if f.download_status != 204:
                    continue
                
            response = check_job(f.job_id)
            f.download_status = response.status_code

            if response.status_code == 200:
                print(f'{f.filename}: [job: {f.job_id}] DOWNLOAD COMPLETE')
                f.xml_data = response.text
                xml_path = save_xml(f, out_dir)
                xml_to_json(f, xml_path, out_dir)
                print(f'{f.filename}: JSON file saved')
            elif response.status_code == 204:
                print(f'{f.filename}: [job: {f.job_id}] ({response.status_code}) NO CONTENT YET')
                f.download_error = response.text
                done = False
            elif response.status_code == 404:
                print(f'{f.filename}: [job: {f.job_id}] ({response.status_code}) NOT FOUND')
                f.download_error = response.text
                done = False
            elif response.status_code == 500:
                print(f'{f.filename}: [job: {f.job_id}] ({response.status_code}) INTERNAL SERVER ERROR')
                f.download_error = response.text
                done = False
            else:
                print(f'{f.filename}:[job: {f.job_id}] ({response.status_code}) {response.text}')
                f.download_error = response.text
                done = False
                
        if monitor_file != None:
            with threading.Lock():
                write_nodes(monitor_file, node_list)

    return node_list


def write_nodes(filename, nodes):
    with open(filename, 'w') as outfile:
        json.dump(nodes, outfile, default=dataNode_to_dict)


def dataNode_to_dict(object):
    """ Helper function for converting the dataNodes to dictionaries
    """
    if isinstance(object, dataNode):
        return object.__dict__
    else:
        raise TypeError("Only dataNodes will be JSON serialized")


def read_tempjson(filename):
    nodes = []
    if isfile(filename):
        with open(filename) as json_file:
            data = json.load(json_file)

        for d in data:
            dn = dataNode(d['fileDir'], d['filename'])
            dn.job_id = d['job_id']
            dn.xml_data = d['xml_data']
            dn.upload_status = d['upload_status']
            dn.upload_error = d['upload_error']
            dn.download_status = d['download_status']
            dn.download_error = d['download_error']

            nodes.append(dn)

    return nodes


def check_job(job_id):
    """Check job status, provided the job ID from API.

    With the job ID provided from the HTTP response, this sends a request on the status of the job.

    Returns: HTTP response from API
    """
    url = "https://api.apptek.com/api/v2/transcribe/"+job_id
    querystring = {"[format":"format]"}
    headers = {"x-token": API_KEY}
    
    return requests.request("GET", url, headers=headers, params=querystring)


def save_xml(node, out_dir):
    """Saves the xml data to a file into a separate directory in the out_dir  
    """
    xmldir_path = join(out_dir, '_xml_output')
    if not isdir(xmldir_path):
        mkdir(xmldir_path)
        
    xmlfile_path = join(str(xmldir_path), node.filename[:len(node.filename)-4]+'.xml')
    
    with open(xmlfile_path, 'w') as fp:
        for line in node.xml_data:
            fp.write(line)

    return xmlfile_path


def xml_to_json(node, xml_path, out_dir):
    output_filepath = join(out_dir, node.filename[:len(node.filename)-4] + '.json')
    subprocess.run(["./TScriptCnv-x86_64", "-in", "asr", xml_path, "-out", "ibm_json", output_filepath, "-o", "dontmerge"], stdout=subprocess.PIPE)


def get_args():
    """Returns the arguments provided in the command line."""
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--model')
    argparser.add_argument('--input')
    argparser.add_argument('--output')
    return argparser.parse_args()


def main():
    args = get_args()
    # upload the input files to api
    node_list = upload_for_transcription(args.input, args.model, None)
    
    # get the xml data for each job
    node_list = get_transcript(node_list, args.output, None)


if __name__ == '__main__':
    print('WE OUT HERE')
    main()
